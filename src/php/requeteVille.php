<?php

require_once('Model.php');
require_once ('Conf.php');

if (isset($_GET['ville'])) {
    $ville = $_GET['ville'];

    $tab = Model::selectByName($ville);

    echo json_encode($tab);
} else {
    echo json_encode(array('error' => 'Paramètre "ville" manquant dans l\'URL.'));
}
?>
