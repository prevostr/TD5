// Fonction pour afficher les villes dans la balise <div id="autocompletion">
function afficheVilles(tableau) {
    videVilles();

    // Sélection de la balise <div id="autocompletion">
    const autocompletionDiv = document.getElementById("autocompletion");

    tableau.forEach(ville => {
        const paragraphe = document.createElement("p");
        paragraphe.textContent = ville;
        autocompletionDiv.appendChild(paragraphe);
    });
}

function videVilles() {
    const autocompletionDiv1 = document.getElementById("autocompletion");
    while (autocompletionDiv1.firstChild) {
        autocompletionDiv1.removeChild(autocompletionDiv1.firstChild);
    }
    // ou
    const autocompletionDiv2 = document.getElementById("autocompletion");
    autocompletionDiv2.innerHTML = ""; // ou autocompletionDiv2.textContent = "";
}

document.addEventListener('DOMContentLoaded', function() {
    // Ajout d'un écouteur d'événement à l'input avec l'événement input
    document.getElementById('ville').addEventListener('input', function() {
        // Appel de maRequeteAJAX avec la valeur de l'input
        maRequeteAJAX(this.value);
    });

    // Ajout d'un écouteur d'événement à la div autocompletion avec l'événement click
    document.getElementById('autocompletion').addEventListener('click', function(event) {
        // Vérification si l'élément cliqué est un paragraphe
        if (event.target.tagName === 'P') {
            // Remplissage de l'input avec le contenu du paragraphe cliqué
            document.getElementById('ville').value = event.target.textContent;
            // Vidage de la div autocompletion
            videVilles();
        }
    });
});

function requeteAJAX(stringVille,callback) {
    let url = "php/requeteVille.php?ville=" + encodeURIComponent(stringVille);
    let requete = new XMLHttpRequest();
    requete.open("GET", url, true);
    requete.addEventListener("load", function () {
        callback(requete);
    });
    requete.send(null);
}

function callback_1(req) {
    console.log(req);
}

function callback_2(xhr) {
    console.log(JSON.parse(xhr.responseText));
}

function callback_3(xhr) {
    let parsedResponse = JSON.parse(xhr.responseText);
    let namesArray = parsedResponse.map(obj => obj.name);

    console.log(namesArray);
}

function callback_4(xhr) {
    let parsedResponse = JSON.parse(xhr.responseText);
    let namesArray = parsedResponse.map(obj => obj.name);
    afficheVilles(namesArray);
}

function maRequeteAJAX(ville) {
    requeteAJAX(ville, callback_4);
}


